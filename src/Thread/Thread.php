<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 14-3-16
 * Time: 22:45
 */

namespace YamiTenshi\ThreadManager\Thread;

use YamiTenshi\ThreadManager\Event\BaseEvent;
use YamiTenshi\ThreadManager\Exceptions\IncompleteThreadException;

/**
 * Class Thread
 *
 * This is the base Thread class, which must be extended for all Threads.
 *
 * The most important part of the thread is the internal logic handler, which _must_ be a generator!
 * The value yielded by the thread must also be NULL whenever the thread is not yet done, but merely
 * gives control back to the thread manager. At the end of its lifetime, a thread's internal logic
 * handler is expected to return an integer status code. A status code of zero will be interpreted
 * as "this thread is finished, and all is well".
 *
 * Any non-zero return code will be treated as an error, and will result in the termination
 * of the current script (or at least all threads managed by the thread manager).
 *
 * Any exception thrown from the thread's internal logic handler will also be handled as an error.
 *
 * @package YamiTenshi\ThreadManager\Thread
 */
abstract class Thread {
    /** @var \Generator This will handle the internal logic, and will be called by run() */
    private $logicHandler = null;

    /** @var int The PID of this thread */
    private $pid;

    /** @var bool Whether the thread has been  */
    private $initalized = false;

    /** @var bool */
    private $firstRun = true;

    /**
     * Thread logic handler initalizer.
     *
     * Declared final to enforce the internal logic handler to ALYWAYS be a generator.
     * The reason for this is that normal functions cannot interrupt their control flow
     * mid-execution, while generators can (even if it's not really what they're meant for).
     *
     * @param \Generator $logicHandler
     */
    final protected function setLogicHandler(\Generator $logicHandler)
    {
        $this->logicHandler = $logicHandler;
        $this->initalized = true;
    }

    /**
     * @param $pid
     */
    final public function setPID($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return int
     */
    final public function getPID()
    {
        return $this->pid;
    }

    /** Perform pre-execution preparations */
    public function init()
    {}

    /**
     * Perform one step of this thread's internal logic, and return its result
     * @return int
     * @throws IncompleteThreadException
     */
    final public function run() {
        if ($this->logicHandler === null || !$this->logicHandler->valid()) {
            throw new IncompleteThreadException('Thread with PID ' . $this->getPID() . ' (' . get_class($this) . ') not yet initalized in ' . __METHOD__ . '!');
        }

        // On first call to current(), the generator will automatically execute up to the first yield
        if (!$this->firstRun) {
            $this->logicHandler->next();
        }
        $this->firstRun = false;

        return $this->logicHandler->current();
    }

    /** Perform post-execution cleanup */
    public function terminate()
    {}

    /**
     * Handle an event
     *
     * @param BaseEvent $event
     */
    abstract public function handleEvent(BaseEvent $event);
}