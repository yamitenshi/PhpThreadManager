<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 21:19
 */

namespace YamiTenshi\ThreadManager\Thread;

use YamiTenshi\ThreadManager\Event\BaseEvent;

/**
 * Class BasicThread
 *
 * A basic thread with a dynamic logic handler that gets passed to the constructor.
 * Does no event handling whatsoever.
 *
 * @package YamiTenshi\ThreadManager\Thread
 */
class BasicThread extends Thread
{
    /**
     * BasicThread constructor.
     * @param \Generator $logicHandler the logic handler to be assigned to this thread
     */
    public function __construct(\Generator $logicHandler)
    {
        $this->setLogicHandler($logicHandler);
    }

    public function handleEvent(BaseEvent $event)
    {}

}