<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 20:31
 */

namespace YamiTenshi\ThreadManager\Exceptions;

/** Exception to indicate a problem that has stopped execution of all threads */
class ExecutionException extends \Exception {};