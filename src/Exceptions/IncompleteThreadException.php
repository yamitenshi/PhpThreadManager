<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 21:09
 */

namespace YamiTenshi\ThreadManager\Exceptions;

/** Exception to indicate that a thread that is attempted to run does not yet have a logic handler assigned */
class IncompleteThreadException extends \Exception {};