<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 20:21
 */

namespace YamiTenshi\ThreadManager\Exceptions;

/** Exception to indicate that an error has occurred with a single thread */
class ThreadErrorException extends \Exception {};