<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 14-3-16
 * Time: 22:46
 */

namespace YamiTenshi\ThreadManager\Event;

/**
 * Class BaseEvent
 *
 * A base class that events to be handled by threads can extend.
 * In its base form, the events only have a name to indicate what happened, but of course
 * implementations can add other data as needed.
 *
 * @package YamiTenshi\ThreadManager\Event
 */
class BaseEvent {
    /** @var string */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}