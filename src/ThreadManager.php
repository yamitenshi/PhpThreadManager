<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 14-3-16
 * Time: 22:45
 */

namespace YamiTenshi\ThreadManager;

use YamiTenshi\ThreadManager\Exceptions\ExecutionException;
use YamiTenshi\ThreadManager\Exceptions\ThreadErrorException;
use YamiTenshi\ThreadManager\Thread\Thread;
use YamiTenshi\ThreadManager\Event\BaseEvent;

class ThreadManager {
    /** @var Thread[] */
    private $threads = [];

    protected function __construct()
    {
    }

    public static function getInstance() {
        static $instance = null;
        if ($instance === null) {
            $instance = new self;
        }

        return $instance;
    }

    /**
     * Add a thread to the thread manager.
     * Returns its key in the internal array, or PID if you will
     *
     * @param Thread $thread
     * @return int
     */
    public function registerThread(Thread $thread)
    {
        $this->threads[] = $thread;
        $thread->init();

        end($this->threads);
        $pid = key($this->threads);
        reset($this->threads);

        $thread->setPID($pid);

        return $pid;
    }

    /**
     * Remove a thread from the manager
     *
     * @param int $pid The PID, as returned by addThread()
     * @throws \InvalidArgumentException if the supplied PID does not exist
     */
    public function removeThread($pid)
    {
        if (isset($this->threads[$pid])) {
            $this->threads[$pid]->terminate();
            unset($this->threads[$pid]);
        } else {
            throw new \InvalidArgumentException("The supplied PID does not exist");
        }
    }

    /**
     * Trigger an event to be handled by all registered threads
     *
     * @param BaseEvent $event
     */
    public function triggerEvent(BaseEvent $event)
    {
        /** @var Thread $thread */
        foreach ($this->threads as $thread) {
            $thread->handleEvent($event);
        }
    }

    /**
     * @throws \Exception
     */
    public function execute() {
        while ($this->threads) {
            /** @var Thread $thread */
            foreach ($this->threads as $pid => $thread) {
                try {
                    $returnCode = $thread->run();
                    if ($returnCode !== null) {
                        if ($returnCode != 0) {
                            throw new ThreadErrorException("Child {$pid} exited with non-zero status code {$returnCode}!", $returnCode);
                        }

                        // Thread is finished
                        $this->removeThread($pid);
                    }
                } catch (\Exception $ex) {
                    $this->cleanup();

                    // We've cleaned up, leave the script to handle what we do with this exception
                    throw new ExecutionException("The thread manager exited with an error status", $ex->getCode(), $ex);
                }
            }
        }

        return;
    }

    /**
     * Terminate all registered threads
     */
    private function cleanup()
    {
        foreach ($this->threads as $pid => $thread) {
            $this->removeThread($pid);
        }
    }
}