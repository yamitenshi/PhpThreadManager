# PHP Thread Manager library

A PHP thread manager. I'd call it a thread scheduler, but the threads themselves decide when to pass control back to the
manager, so that's a bit of an overstatement.

## Why?

Why not? It's possibly the most pointless thing I've ever written (I mean, really, trying to emulate multithreading without
actually multithreading in a language that has no multithreading support is pretty useless and is really only detrimental
to performance).

It was a fun thing to write, however, and I needed a bit of code to show people.

## How?

With PHP 5.5, generators were introduced. They're not really (or really not) intended for this particular use, but it just
so happens you can abuse them as normal functions that pass control back to whatever is "iterating" over them at whatever
point makes sense.

This library abuses that to simulate multiple threads. Threads can freely communicate among each other using events, and
can choose when to pass control back to the manager through `yield`.

## Can I see it in action?

Yes, you can! Head to https://gitlab.com/yamitenshi/PhpThreadManagerShowcase for a simple example project.

## How do I use it?

If you want to include this in your project, you can either clone this repo and include it manually in your autoloading
by defining the `src/` directory as the root for the `YamiTenshi\ThreadManager` namespace,or you can take the easier route
and include

    "repositories": [
        {
            "type" : "vcs",
            "url": "git@gitlab.com:yamitenshi/PhpThreadManager.git"
        }
    ]

in your `composer.json`, and run `composer require yamitenshi/php-thread-manager:dev-master` to have composer do the work
for you.

You can then extend the `YamiTenshi\ThreadManager\Thread\Thread` class to define a thread, or pass a `Generator` to
`YamiTenshi\ThreadManager\Thread\BasicThread`'s constructor.

When extending the `Thread` class, be sure to define a generator to handle the logic using its protected method
`setLogicHandler()` from the constructor or the `init()` method. You can use a method, something from the global namespace,
or anything really, as long as it's a generator. If this is not done, a
`YamiTenshi\ThreadManager\Exceptions\IncompleteThreadException` will be thrown when the thread manager tries to run the thread!

You can then get the `ThreadManager`'s instance by calling `YamiTenshi\ThreadManager\ThreadManager::getInstance()`.
Register a thread by passing it to the thread manager's `registerThread()` method. This method returns the "PID" of the
thread, which can be used to remove it again with the manager's `removeThread()` method. Registering a thread calls its
`init()` method, and removing it calls its `terminate()` method, so neither need to be called manually.

Yielding `null` from your thread's logic handler indicates to the thread manager that the thread is not exiting, but merely
passing control back to the manager. Anything other indicates that the thread is exiting, in which case a non-zero value
is interpreted as an error, and will *terminate all threads currently registered the the manager*. A
`YamiTenshi\ThreadManager\Exceptions\ExcecutionException` will also be thrown, chained from a
`YamiTenshi\ThreadManager\Exceptions\ThreadErrorException` containing more information about what happened specifically.

Threads can handle events by implementing the `handleEvent()` method. Events should identify themselves with a name, and
can be safely ignored if not relevant. Events can be triggered by calling the thread manager's `triggerEvent()` method and
passing it an object extended from `YamiTenshi\ThreadManager\Event\BaseEvent`.

When all threads are properly set up and registered, call the manager's `execute()` method to run all of it.